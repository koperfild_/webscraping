from bs4 import BeautifulSoup
import requests
import time
import re
import ScrapedInfo
import database
import abc
from abc import ABC, abstractmethod
import pandas as pd
import argparse

ASKED = 'asked'
ANSWERED = 'answered'
EDITED = 'edited'
BASE_URL = "http://stackoverflow.com"
#default setting of scraper to scrape 1st k questions from list of questions
DEF_Q_PER_PAGE = 3
#default scraper setting: limit of total scraped questions
DEF_PAGE_NUM_LIM = 9
#-1 because every new question in the beginning increases it by 1 (all posts behavior)
POST_COUNTER = -1

DEFAULT_CSV_FILE_NAME = 'StackOverflow posts info.csv'

class BasePostScraper(ABC):
    """Interface for entity(posts) scrapers"""
    @abstractmethod
    def _get_votes(self, tag_to_scrape):
        """Gets votes count for this post"""
        pass

    @abstractmethod
    def _get_contributors(self, tag_to_scrape):
        """
        Scrapes all contributors of post
        :param tag_to_scrape: tag containing post entity
        :type tag_to_scrape: soup subtree
        #TODO: should be list of all contributors: author, editor, wiki community if present and maybe other
        :return:
        """
        pass

    @abstractmethod
    def scrape(self, tag_to_scrape, relative_position):
        """Scrapes all info from post entity"""
        pass

class CommentPostScraper(BasePostScraper):
    """Comment Scraper"""
    def _parse_reputation(self, reputation_string):
        """Parse author's reputation score"""
        reputation_string = reputation_string.split()[0]
        reputation_string = reputation_string.replace(',','_')
        return int(reputation_string)

    def _get_votes(self, comment_tag):
        votes_tag = comment_tag.select_one('div.comment-score.js-comment-edit-hide > span')
        if votes_tag is None:
            return 0
        votes = votes_tag.text
        #if the comment has no votes
        if len(votes.strip()) == 0:
            votes = None
        else:
            votes = int(votes)
        return votes

    def _get_contributors(self, tag_to_scrape):
        #important:
        """Currently not used"""
        pass


    #TODO: check whether to make _get_contributor returning not list but 1 value. Have made so to be the same\
    # as AnswerScraper and QuestionScraper
    def _get_contributor(self, comment_tag):
        time_tag = comment_tag.select_one('span.comment-date span.relativetime-clean')
        action_time = time_tag['title']
        action_time = time.strptime(action_time, '%Y-%m-%d %H:%M:%SZ')
        #TODO: Have no idea should or not to distinguish comment action type and do we have history of creation and editing
        action_type = ScrapedInfo.ActionType.ANSWER
        user_tag = comment_tag.select_one('a.comment-user')
        user_name = user_tag.text
        user_id = re.search(r'users/(\d+)/', user_tag['href']).group(1)
        user_id = int(user_id)
        reputation = self._parse_reputation(user_tag['title'])
        res = ScrapedInfo.Contributor(user_id, user_name, reputation, action_time, action_type)
        return res

    #TODO: return smth good structured
    def scrape(self, tag_to_scrape, relative_position):
        global POST_COUNTER
        POST_COUNTER += 1
        print("Comment is processing")
        votes_count = self._get_votes(tag_to_scrape)
        author = self._get_contributor(tag_to_scrape, )
        comment_post = ScrapedInfo.Comment(author, None, votes_count, POST_COUNTER, relative_position)
        print("Comment is processed")
        return comment_post


class PostScraper(BasePostScraper):
    def _get_votes(self, question_tag):
        t = question_tag.select_one('div.votecell.post-layout--left')
        t = t.select_one('div.js-vote-count')
        votes = int(t['data-value'])
        return votes

    def _parse_action_time_and_type(self, time_tag):
        """
        Gets the kind of contributor's action (answering or edition) and its time
        :param time_tag: tag containing details
        :return (action_time, action_type)
        """
        if ASKED in time_tag.text:
            action_type = ScrapedInfo.ActionType.ASK
        elif ANSWERED in time_tag.text:
            action_type = ScrapedInfo.ActionType.ANSWER
        else:
            action_type = ScrapedInfo.ActionType.EDITION
        time_tag = time_tag.select_one('span[title]')
        action_time = time_tag['title']
        action_time = time.strptime(action_time, '%Y-%m-%d %H:%M:%SZ')
        return action_time, action_type

    def _parse_user_info(self, user_info_tag, is_author_tag=False):
        """Creates contributor with parsed info about him (scores, badges) and tipe of action, action time"""
        contr = ScrapedInfo.Contributor()
        contr.is_author = is_author_tag
        #get action time and action type
        time_tag = user_info_tag.select_one('div.user-action-time')
        if time_tag:
            contr.action_time, contr.action_type = self._parse_action_time_and_type(time_tag)

        user_details = user_info_tag.select_one('div.user-details')
        user = user_details.select_one('a[href]')
        # important:
        # TODO: Need to decide how to deal with empty contributors
        # https://stackoverflow.com/questions/80476/how-can-i-concatenate-two-arrays-in-java/80559#80559
        # it's shown when author edited Answer/Question. Currently we flush this info
        if user is not None:
            #get user name
            contr.name = user.text
            #get user id
            user_id = re.search(r'users/(-?\d+)/', user['href']).group(1)
            contr.id = int(user_id)
            #get user reputation score
            reputation_tag = user_details.select_one('span.reputation-score')
            reputation_string = reputation_tag['title']
            if len(reputation_string.split()) < 3:
                # cut 'k' from the right because sometimes numbers are represented as 13,2k
                reputation_string = reputation_tag.text.rstrip('k')
            else:
                # We take last word in string of title attribute value
                reputation_string = reputation_string.split()[-1]
            # to cast str to in we replace ',' to '_'. Also we can replace for '' but it's more clear
            reputation = int(reputation_string.replace(',', '_'))
            contr.reputation = reputation
        return contr

    def _get_contributors(self, post_tag):
        """
        Returns (author, editor). If there is no editor in post then (author, None)
        :param post_tag:
        :type post_tag:
        :return:(author, editor)
        """
        container = post_tag.select_one('div.mb0')
        users_tags = container.select('div.post-signature.grid--cell > div.user-info')
        #last in users_tags is author
        author_tag = users_tags[-1]
        author = self._parse_user_info(author_tag, True)
        #TODO: implement scraping of not only author
        editor = None
        if len(users_tags) == 2:
            editor_tag = users_tags[0]
            # important: some info can be missed due to lack in source like on
            # https://stackoverflow.com/questions/80476/how-can-i-concatenate-two-arrays-in-java/80559#80559
            editor = self._parse_user_info(editor_tag)

        return author, editor


    def _get_comments(self, post_tag):
        """
        Scrapes all post entity(Question or Answer) comments
        :param post_tag:
        :type post_tag:
        :return: list of comments
        :rtype: [ScrapedInfo.Comment,ScrapedInfo.Comment,..]
        """
        comments_tags = post_tag.select('div.comments.js-comments-container li.comment.js-comment')
        comments = []
        relative_position = 0
        for com_tag in comments_tags:
            #when we go to next comment we increase absolute_position
            comments.append(CommentPostScraper().scrape(com_tag, relative_position))
            relative_position += 1
        return comments

    def _create_post(self, author, editor, votes_count, abs_pos, relative_position, comments):
        """Creates post entity from all scraped info and contained entities"""
        raise NotImplementedError

    def scrape(self, tag_to_scrape, relative_position):
        global POST_COUNTER
        POST_COUNTER += 1
        abs_pos = POST_COUNTER
        votes_count = self._get_votes(tag_to_scrape)
        #important: editor can be None
        contributor, editor = self._get_contributors(tag_to_scrape)
        comments = self._get_comments(tag_to_scrape)
        post = self._create_post(contributor, editor, votes_count, abs_pos, relative_position, comments)
        return post
        # TODO: make general question statistics scraping from right top corner


class AnswerScraper(PostScraper):
    """Scraper for answer post entity"""
    def _create_post(self, *args):
        return ScrapedInfo.Answer(*args)

    def scrape(self, answer_tag, relative_position):
        print("Answer is processing")
        answer_post = super().scrape(answer_tag, relative_position)
        print("Answer is processed")
        return answer_post

class QuestionScraper(PostScraper):
    """Scraper for question post entity"""
    def _create_post(self, *args):
        return ScrapedInfo.Question(*args)

    def scrape(self, question_tag, relative_position):
        print("Question is processing")
        question_post = super().scrape(question_tag, relative_position)
        print("Question is processed")
        return question_post




class QuestionPageScraper():
    """Scraper for the whole page of topic"""
    def get_topic_id(self, question_tag):
        """Gets question id"""
        return int(question_tag['data-questionid'])

    def get_topic_tags(self, question_tag):
        t = question_tag.select('a.post-tag')
        return [tag_a.string for tag_a in t]

    def get_question_tag(self, soup):
        """Gets tag containing Question"""
        return soup.select_one('div#question')

    def get_answers_tags(self, soup):
        """Returns list of answers tags"""
        #div with id="answers > (nested) div with itemtype = "http://schema.org/Answer"
        answers_tags = soup.select('div#answers div:is(.answer)[itemtype="http://schema.org/Answer"]')
        return answers_tags

    def scrape(self,soup):
        print("Topic is processing")
        question_tag = self.get_question_tag(soup)
        question_id = self.get_topic_id(question_tag)
        topic_tags = self.get_topic_tags(question_tag)
        answers_tags = self.get_answers_tags(soup)
        question_post = QuestionScraper().scrape(question_tag, 0)
        answers = []
        relative_position = 0
        for ans_tag in answers_tags:
            answer_post = AnswerScraper().scrape(ans_tag, relative_position)
            answers.append(answer_post)
            relative_position += 1
        all_scraped_posts = [question_post, *answers]
        scraped_page = ScrapedInfo.Page(question_id, topic_tags, all_scraped_posts)
        return scraped_page

def get_next_page_url(soup):
    """Returns url of next page or None if there is no next page button"""
    next_page_tag = soup.select_one('div.pager.fl span.page-numbers.next')
    if next_page_tag is None:
        return None
    url_tag = next_page_tag.parent
    next_page_url = BASE_URL + url_tag['href']
    return next_page_url

class QuestionsPageScraper():
    def __init__(self, q_per_page, q_limit):
        """
        Main scraper which creates distinct page scraper, calls its scrape method and stores all scraped pages
        :param q_per_page: how much first questions to scrape on search page before pushing next button
        :type q_per_page: int
        :param q_limit: how much to scrape questions in total
        :type q_limit: int
        """
        self.q_per_page = q_per_page
        self.q_limit = q_limit
        self._scraped_pages = None


    def scrape(self, url):
        """
        Starting from url of search page containing urls to topics  scrapes every topic
        on it and goes to the next page of topics. Stores all scraped topics in self._scraped_pages
        :param url: start search page
        """
        curr_questions_page_url = url
        curr_questions_page = requests.get(curr_questions_page_url)
        pages = []
        while len(pages) < self.q_limit and curr_questions_page is not None:
            print("Processing pages from: " + curr_questions_page_url)
            curr_page_soup = BeautifulSoup(curr_questions_page.content, 'html5lib')
            questions_tags = curr_page_soup.select('div#questions div.question-summary h3 a.question-hyperlink')
            questions_urls = [BASE_URL + q_tag['href'] for q_tag in questions_tags]
            #Scrape all urls
            for processing_url in questions_urls[:self.q_per_page]:
                if len(pages) >= self.q_limit:
                    break
                global POST_COUNTER
                POST_COUNTER = -1
                print(processing_url + " is processing")
                curr_scraping_page = requests.get(processing_url)
                url_soup = BeautifulSoup(curr_scraping_page.content, 'html5lib')
                scraped_page = QuestionPageScraper().scrape(url_soup)
                pages.append(scraped_page)
                print(processing_url + " is processed")
            curr_questions_page_url = get_next_page_url(curr_page_soup)
            #Next page button is not found. Exit
            if curr_questions_page_url is None:
                curr_questions_page = None
            else:
                curr_questions_page = requests.get(curr_questions_page_url)
                print("Processed pages from: " + curr_questions_page_url)
        if pages is not None and len(pages) > 0:
            self._scraped_pages = pages
        print("Scraping ended")

    def get_df(self):
        """
        Creates and returns dataframe from all scraped pages
        :return: None if noone page was scraped, otherwise dataframe of scraped by self.scrape() pages
        :rtype:
        """
        if self._scraped_pages is None:
            return None
        df = pd.concat([page.get_df() for page in self._scraped_pages],
              ignore_index=True)
        return df



def main():
    #1st page
    #url = "https://stackoverflow.com/questions/tagged/python"
    #test url (not from the 1st page)

    parser = argparse.ArgumentParser()

    parser.add_argument("-url", type=str, help="Url of the website to parse - Search topic page from stackoverflow website")
    parser.add_argument("-topics_per_page", type=int, help="Number of first topics to scrape on current search page")
    parser.add_argument("-max_topic_count", type=int, help="Maximum number of topics to scrape")

    parser.add_argument("-tocsv", action='store_true', help="Whether to save the scrapped data to csv file")
    parser.add_argument("-csvfilename", type=str, help="Name of the csv file")

    parser.add_argument("-todb", action='store_true', help="Whether to save the scrapped data to a Database")


    #url = "https://stackoverflow.com/questions/tagged/python?sort=newest&page=547&pagesize=50"

    args = parser.parse_args()

    questions_per_page = DEF_Q_PER_PAGE
    max_topics_count = DEF_PAGE_NUM_LIM

    if args.url:
        url = args.url
    if args.topics_per_page:
        questions_per_page = args.topics_per_page
    if args.max_topic_count:
        max_topics_count = args.max_topic_count




    #Create the main scraper
    scraper = QuestionsPageScraper(questions_per_page, max_topics_count)
    #scrape all topics (pages)
    scraper.scrape(url)
    #compose dataframe from scraped pages
    df = scraper.get_df()
    #save dataframe to file
    if args.tocsv:
        if args.csvfilename:
            df.to_csv(args.csvfilename)
        else:
            df.to_csv(DEFAULT_CSV_FILE_NAME)

    print("End")

    if args.todb:
        database.create_db()
        ScrapedInfo.DBWriter(scraper._scraped_pages).write()

if __name__ == "__main__":
    main()

#TODO: 1. Add grequests and logger
#TODO: 2. One of possible filtersScraper of questions page has setting of not considering questions without "Verified" answer
