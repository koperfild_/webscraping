
from bs4 import BeautifulSoup
import requests
import time
import re
import ScrapedInfo

class WebScraper():
    pass

#TODO: Need remake by Julia
class SearchPageScraper():
    def get_questions_list_div(soup):
        """Gets div tag which contains all questions"""
        container = soup.find('div', {'class': 'flush-left'}, {'id': 'questions'})
        post = container.find('div', {'class': 'summary'})
        return post

    def get_urls(soup):
        """Returns list of all questions urls """
        links = []
        for a in soup.find_all('a', {"class": "question-hyperlink"}):
            if a.text:
                links.append("https://stackoverflow.com" + a['href'])
        return links


class CommentScraper():
    """
    Has to collect:
    * date creation
    * whether it was edited or not and how much times
    * author
    * votes
    """
    def __init__(self, soup):
        self.soup = soup

    def get_author_tag(self, post_tag):
        post_tag.find_all()

    def get_votes_tag(self, comment_tag):
        """Returns Votes tag of comment"""
        for vote in comment_tag:
            comment_votes_tag = vote.find("span", {"title": "number of 'useful comment' votes received"})
        return comment_votes_tag

    def get_votes_count(self, comment_tag):
        com_tag = self.get_votes_tag(comment_tag)
        #TODO: parse number
        raise NotImplementedError

    def get_user_info_author(self, answer_tag):
        raise NotImplementedError

    def scrape(self, index, comment_tag):
        pass



class AnswerScraper():
    """
    Has to collect:
    * date creation
    * //DICUSSABLE whether it was edited or not and author of edition
    * author
    * votes
    * whether it's marked as answer by author
    """
    def __init__(self):
        pass
        #raise NotImplementedError


    def get_post_votes_tag(self, soup):
        """Gets tag containing Votes of Question/Answer"""
        post_votes = soup.find("div", {"class": "js-voting-container grid fd-column ai-stretch gs4 fc-black-200"})
        return post_votes

    def get_post_comments_tag(self, soup):
        """Gets tag cocntaining comments to Question/Answer"""
        post_tag = soup.find("ul", {"class": "comments-list js-comments-list"})
        return post_tag

    def get_comments_tags(self, post_tag):
        """Return list of tags post_tag"""
        for comment in post_tag:
            comments_tag = comment.find("div", {"class": "comment-text js-comment-text-and-form"})
        return comments_tag




    def get_author_tag(self, post_tag):
        pass#differs from comment

    def get_editor_tag(self):
        raise NotImplementedError

    #TODO: remake. pass author_tag = self.get_author_tag instead of soup
    def get_user_info_author(self, answer_tag):
        author = answer_tag.find_all("div", class_="user-info")[1].text
        return author

    def get_editor(self, answer_tag):
        editor = answer_tag.find_all("div", class_="user-info")[0]  # user-details + user-action+time
        return editor

    def get_votes_count(self, answer_tag):
        vote_tag = answer_tag.find("div", class_="votecell")
        vote_count = vote_tag.select_one('div[itemprop="upvoteCount"]').string
        return vote_count

    #TODO: remake all get_time methods. Need to get who edited and when.
    #TODO: Don't know what for we need it. Check again difference between question author and editor elements and https://stackoverflow.com/questions/15112125/how-to-test-multiple-variables-against-a-value?fbclid=IwAR3Ao_ia8PmdtpBvjACxq0qY1_5qm6U9LGgGK5g8qO5JlOlJwHwfqdcC394
    def get_time(self, soup):
        time = soup.find("div", {"class": "user-action-time"})
        return time

    def get_time_edited(self, time):
        time_edited = time.find_all("span", {"class": "relativetime"})[0]
        return time_edited

    def get_time_answered(self, time):
        time_answered = time.find_all("span", {"class": "relativetime"})[1]
        return time_answered

    def get_post_contributors(self, answer_tag):
        answer_tag = answer_tag.select('div:is(.answercell, .post-layout--right)')
        users = answer_tag.select('div:is(.user-info, .user-hover)')

        for u in users:
            time_tag = u.select('div:is(.user-action-time)>span[title]')
            action_time = time_tag['title']
            time.strptime(action_time, '%Y-%m-%d %H:%M:%SZ')
            user_tag = u.select('div:is(.user-details)>a[href]')

            #if user_tag has itemprop=




    def scrape(self, index, answer_tag):
        author = self.get_post_contributors(answer_tag)
        author = self.get_user_info_author(answer_tag)
        votes = self.get_votes_count(answer_tag)
        #TODO: make get_creation_date correct
        #creation_date = self.get_creation_date(answer_tag)
        #delete:
        creation_date = time.today()
        # TODO: make get_creation_date correct
        #edited_date = self.get_edition_date(answer_tag)
        #delete:
        edited_date = time.today()
        # place of post after Question/Answer. Only for usual posts, not for Question/Answer
        position = index
        comments_tags = self.get_comments_tags(answer_tag)
        cs = CommentScraper()
        comments = []
        for index, com_tag in enumerate(comments_tags):
            comments.append(cs.scrape(index, com_tag))
        post = ScrapedInfo.Answer(, j, author, votes, creation_date, position, comments
        return post

class QuestionScraper(AnswerScraper):
    def __init__(self):
        pass


    # def scrape(self, question_tag):
    #     raise NotImplementedError


class GeneralScraper():
    def get_date_created(self, tag):
        """
        :param tag: tag that contains tag with our date of topic creation
        """
        date_created = tag.select_one('time[itemprop="dateCreated"]')
        date_created = date_created['datetime']
        date_created = time.strptime(date_created, '%Y-%m-%dT%H:%M:%S')
        return date_created

    def get_last_active_date(self, tag):
        """
        Gets last activity date. Contributor or any other user post/edit activity
        :param tag:higher tag which contains this tag
        """
        last_active_date = tag.find("a", class_="lastactivity-link")
        last_active_date = last_active_date['title']
        last_active_date = time.strptime(last_active_date, '%Y-%m-%d %H:%M:%SZ')
        return last_active_date

    def get_views_count(self, tag):
        """

        :param tag: higher tag which contains this tag with count of views
        :type tag:
        :return:
        :rtype:
        """
        views = tag.find(text=re.compile("\d+,?\d* time?")).string
        # leave only number without "times"
        views = int(views.split()[0].replace(',', ''))
        return views

    def get_question_stats_tag(self, soup):
        """Returns right top tag with statistics: asked, viewed, active"""
        question_starts_tag = soup.select_one("div.module.question-stats")
        return question_starts_tag

    def get_statistics_info(self, soup):
        """
        Gets question stats (shown on the right top of the page)
        :param soup:
        """
        # used css selector
        stats_div = self.get_question_stats_tag(soup)
        date_created = self.get_date_created(stats_div)
        views_count = self.get_views_count(stats_div)
        last_activity = self.get_last_active_date(stats_div)
        #TODO: return smth
        return None

    def get_content_of_question(self, soup):
        content_of_question = soup.find("div", class_="post-text").text
        return content_of_question

    def scrape(self, soup):
        pass

class QuestionPageScraper():
    def __init__(self):
        pass
        #raise NotImplementedError

    def get_question_tag(self, soup):
        """Gets tag containing Question"""
        return soup.select('div#question')



    def _get_answers_block_tag(self, soup):
        """Gets tag containing Answers"""
        answers_tag = soup.select_one('div#answers')
        return answers_tag

    def get_answers_tags(self, soup):
        """Returns list of answers tags"""
        answers_tag = self._get_answers_block_tag(soup)
        return answers_tag.select('div:is(.answer)[itemtype="http://schema.org/Answer"]')


    def get_question_stats_tag(self, soup):
        """Returns right top tag with statistics: asked, viewed, active"""
        return soup.select_one("div.module.question-stats")

    #TODO: remake because impossible to get theme from question_tag
    def find_topic_theme(self, soup):
        question_tag = self.get_question_tag(soup)
        return question_tag.string

    def get_topic_tags_names(self, soup):
        """
        Gets list of mentioned by author tags
        :param soup:
        :type soup:
        :return:
        :rtype:
        """
        # search by class
        # question_tag = soup.find("div", class_="question")
        # search by id
        question_tag = soup.select_one("div#question")
        mentioned_tags = question_tag.select('a[rel="tag"]')
        return [tag.string for tag in mentioned_tags]

    def scrape(self, soup):
        question_tag = self.get_question_tag(soup)
        answers_tags = self.get_answers_tags(soup)
        res = []
        #There can be set some settings for scrapers
        #~~~~~~~~~~~~~~~~~~~~~~~QUESTION PART~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        qs = QuestionScraper()
        question = qs.scrape(2, question_tag)
        #~~~~~~~~~~~~~~~~~~~~~~~ANSWERS AND COMMENTS PART~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ans = AnswerScraper()
        cs = CommentScraper()
        #TODO: replace with real data structure
        some_structure = qs.scrape(question_tag)
        answers = []
        for a_tag in answers_tag:
            #TODO: check return of scrape
            answer = ans.scrape(a_tag)
            answers.append(answer)
        #~~~~~~~~~~~~~~~~~~~~~~~OTHERS PART~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        gs = GeneralScraper()
        general_statistics = gs.scrape(soup)
        #important: question and answers are of type ScrapedInfo.Post if there won't be difference between Comment/Answer/Question classes
        return question, answers, general_statistics







    #TODO: may be useful
    #
    # def get_content_of_question(self, soup):
    #     return soup.find("div", class_="post-text").text
    #
    # def get_user_info_author(self, soup):
    #     return soup.find_all("div", class_="user-info")[1].text
    #
    # def get_user_info_reponse(self, soup):
    #     return soup.find_all("div", class_="user-info")[0]  # user-details + user-action+time

    def get_next_questions_page(self, soup):
        """ get url of the next page """
        try:
            next = soup.find("a", {"rel": "next"}).get("href")
            next_url = "https://stackoverflow.com" + next
            return next_url
        except:
            print("This is the last page!")
            return 1


    #Don't know if I need it
    # def get_question_tag(self, soup):
    #     """Gets tag containing Question"""
    #     question_tag = soup.find("div", {"class": "question"})
    #     return question_tag