from bs4 import BeautifulSoup
import requests
import time
import re
from WebScraper import SearchPageScraper, QuestionPageScraper

#Question is the most first post
#Answer is big post
#comment is small post under answer


SEARCH_TAGS_URLS =['https://stackoverflow.com/questions/tagged/python', 'https://stackoverflow.com/search?q=webscraping']

def make_dataframe(dataobject):
    pass


def main():
    pass
    scraper = QuestionPageScraper(*some settings)
    for url in SEARCH_TAGS_URLS:

        curr_tag_first_page = requests.get(url)
        # gets next questions page by requesting "next" page button's url
        curr_questions_page = curr_tag_first_page
        while curr_questions_page is not None:
            #TODO: define whether we pass BeautifulSoup(page.content, 'html.parser') or papge
            questions_urls = SearchPageScraper().get_urls(curr_questions_page)
            for url in questions_urls:
                page = requests.get(url)
                soup = BeautifulSoup(page.content, 'html.parser')
                data = scraper.scrape(soup)
            #TODO: every page we can add scraped data to dataframe
            #df.append(data)
            curr_questions_page = get_next_questions_page(initial_url)

    # #~~~~~~~~~~~~~~~~~~~~~~~~Legacy code~~~~~~~~~~~~~~~~~~~~~~~~~
    # page = requests.get("https://stackoverflow.com/questions/tagged/python?sort=frequent&pageSize=50&fbclid=IwAR2UvPhQ55SqWbtBmut5t-6I5oFLOF2AI_Ia_7MPAN9Jt8oEIHdA6lLSys0")
    # #page = requests.get("https://stackoverflow.com/questions/18966368/python-beautifulsoup-scrape-tables")
    # soup = BeautifulSoup(page.content, 'html.parser')
    # # question = find_question_header(soup)
    # # topic_tags = get_topic_tags_names(soup)
    # # num = get_upvote_count(soup)
    # # date_created = get_statistics_info(soup)
    # # content = get_content_of_question(soup)
    # # author = get_user_info_author(soup)
    # # user = get_user_info_reponse(soup)
    #
    # links = get_urls(soup)
    # print(links)
    # ~~~~~~~~~~~~~~~~~~~~~~~~End~~~~~~~~~~~~~~~~~~~~~~~~~

def main_test():
    #page = requests.get("https://stackoverflow.com/questions/18966368/python-beautifulsoup-scrape-tables")
    page = requests.get("https://stackoverflow.com/questions/952914/how-to-make-a-flat-list-out-of-list-of-lists")
    soup = BeautifulSoup(page.content, 'html5lib')
    QuestionPageScraper().scrape(soup)

if __name__ == "__main__":
    #main()
    main_test()



