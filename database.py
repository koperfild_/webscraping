import os
import sqlite3
import tqdm
import csv
import io
import ScrapedInfo


def create_db():
    DB_FILENAME = 'Scrapping.db'

    if os.path.exists(DB_FILENAME):
        os.remove(DB_FILENAME)


    with sqlite3.connect(DB_FILENAME) as con:
        cur = con.cursor()
        cur.execute('''CREATE TABLE Post (
                        post_id INT PRIMARY KEY,
                        page_id INT,
                        author_id INT, 
                        post_type CHAR,
                        abs_position INT, 
                        relative_position INT,
                        votes INT, 
                        date_year INT,
                        date_month INT,
                        date_day INT) ''')


        cur.execute('''CREATE TABLE User (
                         user_id INT PRIMARY KEY,
                         id_scraped INT,
                         name CHAR,
                         reputation INT,
                         action_type CHAR,
                         gold_badge INT, 
                         silver_badge INT, 
                         bronze_badge INT)''')

        cur.execute('''CREATE TABLE Tag (
                                     tag_name CHAR PRIMARY KEY)''')

        cur.execute('''CREATE TABLE Post_to_tag (
                                     post_id INT,
                                     tag_name CHAR, 
                                     FOREIGN KEY (post_id) REFERENCES Post(post_id), 
                                     FOREIGN KEY (tag_name) REFERENCES Tag(tag_name))''')

        cur.execute('''CREATE TABLE Post_to_user (
                                     user_id INT,
                                     post_id INT, 
                                     FOREIGN KEY (user_id) REFERENCES User(user_id), 
                                     FOREIGN KEY (post_id) REFERENCES Post(post_id))''')

        cur.execute('''CREATE TABLE Post_to_comment (
                                     post_id INT,
                                     comment_id INT, 
                                     FOREIGN KEY (post_id) REFERENCES Post(post_id))''')


        con.commit()
        cur.close()


def main():
    create_db()

if __name__ == '__main__':
    main()
