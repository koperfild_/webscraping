"""Data structures to store scraped from question page info"""
from enum import Enum
import pandas as pd
import numpy as np
import sqlite3

DB_FILENAME = 'Scrapping.db'

class ActionType(Enum):
    """Enum of types of action. There are only answers and editions"""
    ASK = 0
    ANSWER = 1
    EDITION = 2

class Contributor():
    """Represents contributor to the post. It can be author, editor or wiki community (not implemented)"""
    def __init__(self, id=None, name=None, reputation=None, action_time=None, action_type=None, is_author=False, gold_badges=None, silver_badges=None, bronze_badges=None):
        self.is_author = is_author
        self.id = id
        self.name = name
        self.reputation = reputation
        self.action_time = action_time
        self.action_type = action_type
        self.gold_badges = gold_badges
        self.silver_badges = silver_badges
        self.bronze_badges = bronze_badges


    #Headers of part of the post row. Firstly Author columns, then Editor columns
    #Can be extended also to 3rd contributor like wiki community or smth else
    Headers = ["Author_ID", "Author_Name", "Author_Reputation", "Author_Action_Time", "Author_Action_Type", "Author_Gold_Badges", "Author_Silver_Badges", "Author_Bronze_Badges",
            "Editor_ID", "Editor_Name", "Editor_Reputation", "Editor_Action_time", "Editor_Action_Type", "Editor_Gold_Badges", "Editor_Silver_Badges", "Editor_Bronze_Badges"]

    def asdict(self):
        tmp_dict= {"ID": self.id, "Name": self.name, "Reputation": self.reputation, "Action_Time": self.action_time,  "Action_Type" : self.action_type, "Gold_Badges": self.gold_badges,
            "Silver_Badges": self.silver_badges, "Bronze_Badges": self.bronze_badges}
        return tmp_dict

    @staticmethod
    def extract_as_row(contributor):
        """
        Returns list of values of contributor. Not made via dictionary because if we will have a lot info in posts row
        some column names can have the same values and this dictionary filling will overwrite some other values +
        troubles with length of values list and columns list
        :param contributor: contributor to extract
        :return: list of extracted fields
        """
        row = None
        if not contributor:
            #create contributor with default values
            c = Contributor()
            row = c._extract_as_row()
        else:
            row = contributor._extract_as_row()
        return row

    def _extract_as_row(self):
        """
        extracts all contributor's attributes to the list of values
        """
        return [self.id, self.name, self.reputation, self.action_time, self.action_type, self.gold_badges, self.silver_badges, self.bronze_badges]


class Post():
    """
    Represents entity of post.
    The post is Comment(small posts under question or answer)/Question Post/Answer Post
    """
    def __init__(self, author, editor, votes, abs_position, relative_position, comments=None):#, edited_date=None):
        """
        Comments = None for comments
        For Answer absolute position is the number of all answers and comments in front of it,
        :param author:
        :type author:
        :param editor:
        :type editor:
        :param author:
        :type author:
        :param votes: quantity of votes of post (from the left from post)
        :type votes:
        :type position: place of post after Question/Answer. Only for usual posts, not for Question/Answer
        :param comments: for comment entity(inherited from Post) it's None always as they can't have comments
        """
        self.author = author
        self.editor = editor
        self.votes = votes
        #place of post after Question/Answer. Only for usual posts, not for Question/Answer
        self.abs_position = abs_position
        self.relative_position = relative_position
        self.comments = comments


    def extract_as_row(self):
        """
        Extracts as list of values attributes' values and also extracts info of subentities
        such as contributors (author and editor)
        :return:
        :rtype:
        """
        fields = [self.__class__.__name__, self.votes, self.abs_position, self.relative_position, len(self.comments)]
        auth = Contributor.extract_as_row(self.author)
        ed = Contributor.extract_as_row(self.editor)
        full_row = fields + auth + ed
        return full_row

    @staticmethod
    def extract_headers():
        """
        Extracts post attributes' values. Also add attributes' values of subentities such as contributors (author, editor)
        :return: list of column names\
        """
        return ["Post_Type", "Votes", "Absolute_Pos", "Relative_Pos", "Comments_Count"] + Contributor.Headers


class Page():
    """Entity representing scraping page (Topic on stackoverflow.com)"""
    def __init__(self, question_id, topic_tags, posts):
        """Glues post entities related to page"""
        self.page_id = question_id
        self.posts = posts
        self.topic_tags = topic_tags

    def get_df(self):
        """
        Composes dataframe representing page entity
        :return: dataframe with each row representing post on scraped page
        """
        df = pd.DataFrame()
        headers = Post.extract_headers()
        rows = []
        for post in self.posts:

            row = [self.page_id, ','.join(self.topic_tags), *post.extract_as_row()]
            rows.append(row)
        return pd.DataFrame(np.array(rows), columns=["Page_ID", "Tags", *headers])

class DBWriter():
     def __init__(self, pages):
         self._pages = pages

     def write(self):
         with sqlite3.connect(DB_FILENAME) as con:
             cur = con.cursor()
             counter = 0
             for page in self._pages:

                 cur.executemany("INSERT OR IGNORE INTO Tag (tag_name) VALUES (?)", [(tag,) for tag in page.topic_tags])

                 for post in page.posts:

                     #TODO: add all columns
                     post_field = [page.page_id, post.__class__.__name__, post.abs_position, post.relative_position, post.votes, post.author.asdict()['Action_Time'][0], post.author.asdict()['Action_Time'][1],post.author.asdict()['Action_Time'][2]]
                     cur.execute("INSERT INTO Post (page_id, post_type, abs_position, relative_position, votes, date_year, date_month, date_day) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                                 post_field)

                     post_id = cur.lastrowid

                     for comment in post.comments:
                         comment_field = [page.page_id, comment.__class__.__name__, comment.abs_position, comment.relative_position,
                                       comment.votes, comment.author.asdict()['Action_Time'][0],
                                       comment.author.asdict()['Action_Time'][1], comment.author.asdict()['Action_Time'][2]]
                         cur.execute("INSERT OR IGNORE INTO Post (page_id, post_type, abs_position, relative_position, votes, date_year, date_month, date_day) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                                     comment_field)
                         comment_id = cur.lastrowid

                         cur.execute("INSERT OR IGNORE INTO Post_to_comment (post_id, comment_id) VALUES (?, ?)", [post_id, comment_id])



                     cur.executemany("INSERT OR IGNORE INTO Post_to_tag (post_id, tag_name) VALUES (?, ?)",
                     [(post_id, tag,) for tag in page.topic_tags])

                     user_field = [post.author.asdict()['ID'], post.author.asdict()['Name'], post.author.asdict()['Reputation'], post.author.asdict()['Action_Type'].name, post.author.asdict()['Gold_Badges'], post.author.asdict()['Silver_Badges'], post.author.asdict()['Bronze_Badges']]

                     cur.execute("INSERT OR IGNORE INTO User (id_scraped, name, reputation, action_type, gold_badge, silver_badge, bronze_badge) VALUES (?, ?, ?, ?, ?, ?, ?)",
                            user_field)


                     cur.execute("INSERT OR IGNORE INTO Post_to_user (user_id, post_id) VALUES (?, ?)",
                     [post.author.asdict()['ID'], page.page_id])



                     #write page tags
                 counter += 1
                 if counter % 10 == 0:
                     con.commit()

class Comment(Post):
    """Entity representing comment (small replies under Question or Answer in topic"""
    pass

#TODO: Don't like it
class Answer(Post):
    """Entity representing Answer post"""
    def __init__(self, *args):
        super().__init__(*args)


class Question(Post):
    """Entity representing Question post of the topic"""
    def __init__(self, *args):
        super().__init__(*args)
